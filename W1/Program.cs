﻿using W1;
using W1.DB;

Console.WriteLine("1 - вывод всех записей таблиц");
Console.WriteLine("2 - внесение записей в таблицу");
Console.WriteLine("3 - выход");

var exit = false;

while (true)
{
    Console.WriteLine("Выберите операцию:");
    switch (Console.ReadLine())
    {
        case "1": PrintTable(); break;
        case "2": AddRecord(); break;
        case "3": exit = true; break;
        default: break;
    }
    if (exit) break;
}







void PrintTable()
{
    var dbRep = new DBRepo();

    Console.WriteLine("Table Account");
    Console.WriteLine("{0,3} {1,20} {2,20} {3,20} {4,20} {5,20}", "id", "login", "password", "firstName", "lastName", "middleName");
    var acc = dbRep.GetAcc();
    foreach (var a in acc)
    {
        Console.WriteLine("{0,3} {1,20} {2,20} {3,20} {4,20} {5,20}",
            a.Id.ToString(),
            a.Login,
            a.Password,
            a.FirstName,
            a.LastName,
            a.MiddleName);
    }

    Console.WriteLine("Table Order");
    Console.WriteLine("{0,3} {1,20} {2,20} {3,20} {4,20}", "id", "buyPrice", "count", "idAccount", "idMerchandise");
    var ord = dbRep.GetOrd();
    foreach (var o in ord)
    {
        Console.WriteLine("{0,3} {1,20} {2,20} {3,20} {4,20}",
            o.Id.ToString(),
            o.BuyPrice,
            o.Count,
            o.IdAccount,
            o.IdMerchandise);
    }

    Console.WriteLine("Table Merchandise");
    Console.WriteLine("{0,3} {1,20} {2,20} {3,20} {4,20}", "id", "name", "sellCount", "price", "idAccount");

    var ss = dbRep.GetMerch();
    foreach (var s in ss)
    {
        Console.WriteLine("{0,3} {1,20} {2,20} {3,20} {4,20}",
            s.Id.ToString(),
            s.Name,
            s.SellCount,
            s.Price,
            s.IdAccount);
    }
    dbRep = null;
}

void AddRecord()
{
    Console.WriteLine("1 - Accounts");
    Console.WriteLine("2 - Orders");
    Console.WriteLine("3 - Merchandises");
    Console.WriteLine("4 - выход");

    var exitCreate = false;
    while (true)
    {
        Console.WriteLine("Выберите таблицу:");
        switch (Console.ReadLine())
        {
            case "1":
                {
                    var acc = new Account();

                    acc.Login = ReadStrNotEmpty("Логин");
                    acc.Password = ReadStrNotEmpty("Пароль");
                    acc.FirstName = ReadStrNotEmpty("Имя");
                    acc.LastName = ReadStrNotEmpty("Фамилию");

                    Console.WriteLine("Введите отчество");
                    acc.MiddleName = Console.ReadLine();

                    var dbRep = new DBRepo();
                    dbRep.AddAcc(acc);
                    dbRep = null;
                    Console.WriteLine("Запись внесена");
                    break;
                }
            case "2":
                {
                    var ord = new Order();

                    var dbRep = new DBRepo();

                    var acc = dbRep.GetAcc(ReadStrNotEmpty("Логин"));
                    if (acc is null)
                    {
                        Console.WriteLine("Пользователь не найден");
                        break;
                    }
                    ord.IdAccount = acc.Id;

                    var merch = dbRep.GetMerch(ReadStrNotEmpty("Товар"));
                    if (merch is null)
                    {
                        Console.WriteLine("товар не найден");
                        break;
                    }
                    ord.IdMerchandise = merch.Id;

                    Console.WriteLine("Введите цену");
                    int price;
                    Int32.TryParse(Console.ReadLine(), out price);
                    ord.BuyPrice = price;

                    Console.WriteLine("Введите количество");
                    int count;
                    Int32.TryParse(Console.ReadLine(), out count);
                    ord.Count = count;

                    dbRep.AddOrd(ord);
                    dbRep = null;
                    Console.WriteLine("Запись внесена");
                    break;
                }
            case "3":
                {
                    var merch = new Merchandise();

                    var dbRep = new DBRepo();

                    var acc = dbRep.GetAcc(ReadStrNotEmpty("Логин"));
                    if (acc is null)
                    {
                        Console.WriteLine("Пользователь не найден");
                        break;
                    }
                    merch.IdAccount = acc.Id;

                    merch.Name = ReadStrNotEmpty("наименование товара");
                    Console.WriteLine("Введите цену");
                    int price;
                    Int32.TryParse(Console.ReadLine(), out price);
                    merch.Price = price;

                    Console.WriteLine("Введите количество");
                    int count;
                    Int32.TryParse(Console.ReadLine(), out count);
                    merch.SellCount = count;

                    dbRep.AddMerch(merch);
                    dbRep = null;
                    Console.WriteLine("Запись внесена");
                    break;
                }
            case "4": exitCreate = true; break;
            default: break;
        }
        if (exitCreate) break;
    }
}

string ReadStrNotEmpty(string descr)
{
    Console.WriteLine("Введите {0}", descr);

    var srt = "";
    while (srt == "")
    {
        srt = Console.ReadLine();

        if (String.IsNullOrWhiteSpace(srt) || String.IsNullOrEmpty(srt))
        {
            srt = "";
            Console.WriteLine("Строка не может быть пустой");
        }
    }
    return srt;
}