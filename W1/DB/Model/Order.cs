﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace W1.DB
{
    public class Order
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("buyPrice")]
        public decimal BuyPrice { get; set; }
        [Column("count")]
        public long Count { get; set; }

        [Column("idAccount")]
        public long IdAccount { get; set; }
        [Column("idMerchandise")]
        public long IdMerchandise { get; set; }
    }
}
