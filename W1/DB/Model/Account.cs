﻿using System.ComponentModel.DataAnnotations.Schema;

namespace W1.DB
{
    public class Account
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("login")]
        public string Login { get; set; }

        [Column("password")]
        public string Password { get; set; }

        [Column("firstName")]
        public string FirstName { get; set; }

        [Column("lastName")]
        public string LastName { get; set; }

        [Column("middleName")]
        public string MiddleName { get; set; }
    }
}
