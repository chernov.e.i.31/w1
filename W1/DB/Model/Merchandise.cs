﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace W1.DB
{
    public class Merchandise
    {
        [Column("id")]
        public virtual long Id { get; set; }

        [Column("name")]
        public virtual string Name { get; set; }

        [Column("sellCount")]
        public virtual long SellCount { get; set; }
        [Column("price")]
        public virtual decimal Price { get; set; }
        [Column("idAccount")]
        public virtual long IdAccount { get; set; }
    }
}
