﻿using W1.DB;

namespace W1
{

    public class DBRepo
    {
        const string connectionString = "Host=localhost;port=5432;Username=postgres;Password=12345;Database=ebay";

        public List<Account> GetAcc()
        {
            using (var con = new DBConnector(connectionString))
            {
                return con.Accounts.ToList();
            }
        }

        public List<Order> GetOrd()
        {
            using (var con = new DBConnector(connectionString))
            {
                return con.Orders.ToList();
            }
        }

        public List<Merchandise> GetMerch()
        {
            using (var con = new DBConnector(connectionString))
            {
                return con.Merchandises.ToList();
            }
        }

        public Account GetAcc(string _login)
        {
            using (var con = new DBConnector(connectionString))
            {
                return con.Accounts.Where(x=> x.Login== _login).FirstOrDefault();
            }
        }

        public Merchandise GetMerch(string _name)
        {
            using (var con = new DBConnector(connectionString))
            {
                return con.Merchandises.Where(x => x.Name == _name).FirstOrDefault();
            }
        }

        public void AddAcc(Account account)
        {
            using (var con = new DBConnector(connectionString))
            {
                account.Id = con.Accounts.Max(x => x.Id) + 1;
                con.Accounts.Add(account);
                con.SaveChanges();
            }
        }

        public void AddOrd(Order order)
        {
            using (var con = new DBConnector(connectionString))
            {
                order.Id = con.Orders.Max(x => x.Id) + 1;
                con.Orders.Add(order);
                con.SaveChanges();
            }
        }

        public void AddMerch(Merchandise merchandise)
        {
            using (var con = new DBConnector(connectionString))
            {
                merchandise.Id = con.Merchandises.Max(x => x.Id) + 1;
                con.Merchandises.Add(merchandise);
                con.SaveChanges();
            }
        }
    }
}
