﻿using Microsoft.EntityFrameworkCore;
using W1.DB;

namespace W1
{
    public class DBConnector : DbContext
    {
        private string connectionString;

        public DBConnector(string _connectionString)
        {
            connectionString = _connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(connectionString);
        }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Order> Orders { get; set; }
        public DbSet<Merchandise> Merchandises { get; set; }
    }
}